package net.ddns.endercrypt.bsm;

import java.util.Map;

public abstract class StringMap<V> implements Map<String, V>
{
	@Deprecated
	public boolean containsKey(Object key)
	{
		return containsKey((String) key);
	}

	public abstract boolean containsKey(String key);

	@Deprecated
	public boolean containsValue(Object value)
	{
		return containsValue((String) value);
	}

	public abstract boolean containsValue(String value);

	@Deprecated
	public V get(Object key)
	{
		return get((String) key);
	}

	public abstract V get(String key);

	@Deprecated
	public V remove(Object key)
	{
		return remove((String) key);
	}

	public abstract V remove(String key);
}
