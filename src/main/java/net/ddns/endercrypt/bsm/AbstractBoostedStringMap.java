package net.ddns.endercrypt.bsm;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

public abstract class AbstractBoostedStringMap<V> extends StringMap<V>
{
	@Override
	public boolean isEmpty()
	{
		return size() == 0;
	}

	@Override
	public void putAll(Map<? extends String, ? extends V> map)
	{
		for (Entry<? extends String, ? extends V> entry : map.entrySet())
		{
			String key = entry.getKey();
			V value = entry.getValue();

			put(key, value);
		}
	}

	@Override
	public Set<String> keySet()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public Collection<V> values()
	{
		throw new UnsupportedOperationException();
	}
}
