package net.ddns.endercrypt.bsm.impl;

import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import net.ddns.endercrypt.bsm.character.CharacterArray;
import net.ddns.endercrypt.bsm.character.CharacterSetException;

public class CharacterNode
{
	private CharacterArray characterArray;
	private CharacterNode parent;
	private char character;

	private CharacterNode[] childNodes;
	private Object object;

	private int size = 0;

	public CharacterNode(CharacterArray characterArray, CharacterNode parent, char character)
	{
		this.characterArray = characterArray;
		this.parent = parent;
		this.character = character;

		childNodes = new CharacterNode[characterArray.size()];
	}

	public int size()
	{
		return size;
	}

	public CharacterNode getNode(String text, int index, boolean createIfNotExists)
	{
		if (text.length() == index)
		{
			return this; // reached end
		}
		else
		{
			char character = text.charAt(index);
			int childNodeIndex = characterArray.indexOf(character);
			if (childNodeIndex <= 0)
			{
				throw new CharacterSetException("Character '" + character + "' not found in the supplied "
						+ CharacterArray.class.getSimpleName());
			}
			CharacterNode node = childNodes[childNodeIndex];
			if (node == null)
			{
				if (createIfNotExists)
				{
					node = childNodes[childNodeIndex] = new CharacterNode(characterArray, this, character);
				}
				else
				{
					return null;
				}
			}
			return node.getNode(text, index + 1, createIfNotExists);
		}
	}

	private void changeSize(int value)
	{
		size += value;
		if (parent == null)
		{
			return;
		}
		parent.changeSize(value);
	}

	public Object getObject()
	{
		return object;
	}

	public String getKey()
	{
		List<CharacterNode> nodes = new ArrayList<>();
		nodes.add(this);
		while (true)
		{
			CharacterNode current = nodes.get(0);
			CharacterNode parent = current.parent;
			if (parent == null)
			{
				break;
			}
			nodes.add(0, parent);
		}
		return nodes.stream().map(n -> String.valueOf(n.character)).collect(Collectors.joining(""));
	}

	public void setObject(Object object)
	{
		if ((this.object == null) && (object != null)) // +1
		{
			changeSize(+1);
		}
		if ((this.object != null) && (object == null)) // -1
		{
			changeSize(-1);
		}
		this.object = object;
	}

	public boolean containsObject(Object object)
	{
		if (this.object == object)
		{
			return true;
		}
		for (CharacterNode characterNode : childNodes)
		{
			if (characterNode != null)
			{
				if (characterNode.containsObject(object))
				{
					return true;
				}
			}
		}
		return false;
	}

	@SuppressWarnings("unchecked")
	public <V> List<Entry<String, V>> entries()
	{
		// Entry<String, V> test = new AbstractMap.SimpleImmutableEntry<>(key, value);
		List<Entry<String, V>> result = new ArrayList<>();
		if (object != null)
		{
			System.out.println("key: " + getKey());
			result.add(new AbstractMap.SimpleImmutableEntry<>(getKey(), (V) object));
		}
		for (CharacterNode characterNode : childNodes)
		{
			if (characterNode != null)
			{
				result.addAll(characterNode.entries());
			}
		}
		return result;
	}
}
