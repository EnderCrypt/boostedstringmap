package net.ddns.endercrypt.bsm.impl;

import java.util.Set;

import net.ddns.endercrypt.bsm.AbstractBoostedStringMap;
import net.ddns.endercrypt.bsm.character.CharacterArray;

public class BoostedStringMap<V> extends AbstractBoostedStringMap<V>
{
	private CharacterArray characterArray;

	private CharacterNode root;

	public BoostedStringMap(CharacterArray characterArray)
	{
		this.characterArray = characterArray;

		clear();
	}

	@Override
	public void clear()
	{
		root = new CharacterNode(characterArray, null, (char) 0);
	}

	@Override
	public int size()
	{
		return root.size();
	}

	@SuppressWarnings("unchecked")
	@Override
	public V put(String key, V value)
	{
		CharacterNode node = root.getNode(key, 0, true);
		V result = (V) node.getObject();
		node.setObject(value);
		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public V get(String key)
	{
		CharacterNode node = root.getNode(key, 0, false);
		if (node == null)
		{
			return null;
		}
		return (V) node.getObject();
	}

	@SuppressWarnings("unchecked")
	@Override
	public V remove(String key)
	{
		CharacterNode node = root.getNode(key, 0, false);
		if (node == null)
		{
			return null;
		}
		V result = (V) node.getObject();
		node.setObject(null);
		return result;
	}

	@Override
	public Set<Entry<String, V>> entrySet()
	{
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean containsKey(String key)
	{
		return (root.getNode(key, 0, false) != null);
	}

	@Override
	public boolean containsValue(String value)
	{
		return root.containsObject(value);
	}
}
