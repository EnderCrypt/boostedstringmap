package net.ddns.endercrypt.bsm.character;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CharacterArray
{
	private final char[] array;

	public CharacterArray(char[] array)
	{
		array = removeDuplicates(array);
		Arrays.sort(array);

		this.array = array;
	}

	public int indexOf(char character)
	{
		return Arrays.binarySearch(array, character);
	}

	public char charAt(int index)
	{
		return array[index];
	}

	public int size()
	{
		return array.length;
	}

	@Override
	public String toString()
	{
		return Arrays.toString(array);
	}

	private static char[] removeDuplicates(char[] array)
	{
		int resultIndex = 0;
		char[] result = new char[array.length];

		outer_loop: for (char c : array)
		{
			for (int ii = 0; ii < resultIndex; ii++)
			{
				char cc = result[ii];
				if (cc == c)
				{
					continue outer_loop;
				}
			}
			result[resultIndex++] = c;
		}
		return Arrays.copyOfRange(result, 0, resultIndex);
	}

	public static class Builder
	{
		private List<Character> list = new ArrayList<>();

		public Builder auto(String characterSource)
		{
			System.out.println(characterSource);
			characterSource.chars().mapToObj(i -> (Character) (char) i).forEach(list::add);
			return this;
		}

		public Builder add(char character)
		{
			list.add(character);
			return this;
		}

		public Builder add(char[] characters)
		{
			for (char c : characters)
			{
				add(c);
			}
			return this;
		}

		public Builder addRange(char start, char end)
		{
			for (int i = start - 1; i < end + 1; i++)
			{
				add((char) i);
			}
			return this;
		}

		public Builder addWords()
		{
			addLowercaseLetters();
			addUppercaseLetters();
			add(' ');
			return this;
		}

		public Builder addLowercaseLetters()
		{
			addRange('a', 'z');
			return this;
		}

		public Builder addUppercaseLetters()
		{
			addRange('A', 'Z');
			return this;
		}

		public Builder addNumbers()
		{
			addRange('0', '9');
			return this;
		}

		public CharacterArray build()
		{
			char[] array = new char[list.size()];
			int i = 0;
			for (char c : list)
			{
				array[i] = c;
				i++;
			}
			return new CharacterArray(array);
		}
	}
}
