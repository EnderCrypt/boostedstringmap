package net.ddns.endercrypt.bsm.character;

@SuppressWarnings("serial")
public class CharacterSetException extends RuntimeException
{
	public CharacterSetException()
	{
		// TODO Auto-generated constructor stub
	}

	public CharacterSetException(String message)
	{
		super(message);
		// TODO Auto-generated constructor stub
	}

	public CharacterSetException(Throwable cause)
	{
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public CharacterSetException(String message, Throwable cause)
	{
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public CharacterSetException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace)
	{
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
