import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

import net.ddns.endercrypt.bsm.character.CharacterArray;
import net.ddns.endercrypt.bsm.impl.BoostedStringMap;

public class Main
{
	public static final int RANGE = 10_000_000;
	public static final int TEST_INTENSITY = 10_000_000;

	public static void main(String[] args) throws IOException
	{
		Map<String, String> normalMap = new HashMap<>();
		populate(normalMap);

		Map<String, String> boostedMap = new BoostedStringMap<>(new CharacterArray.Builder().addNumbers().build());
		populate(boostedMap);

		if (normalMap.size() != boostedMap.size())
		{
			System.err.println("Fail: wrong size");
			System.exit(1);
		}

		System.out.println("Testing...");
		System.out.println("normal:  " + performanceTest(normalMap) + " ms");
		System.out.println("boosted: " + performanceTest(boostedMap) + " ms");
	}

	private static void populate(Map<String, String> map)
	{
		for (int i = 0; i < RANGE; i++)
		{
			String key = String.valueOf(i);
			map.put(key, "KEY_" + key);
		}
	}

	public static long performanceTest(Map<String, String> map)
	{
		long time = System.currentTimeMillis();

		for (int i = 0; i < TEST_INTENSITY; i++)
		{
			String key = String.valueOf((int) Math.floor(Math.random() * 10_000));
			String value = map.get(key);
			String expectedValue = "KEY_" + key;
			if (Objects.equals(value, expectedValue) == false)
			{
				System.err.println("WRONG VALUE: key=" + key + " value=" + value);
			}
		}

		return (System.currentTimeMillis() - time);
	}
}
